from .event import Event3

class ThrowInEvent(Event3):
	NAME = "throw-in"

	def perform(self):
		if self.object not in self.actor:
			self.add_prop("object-not-in-inventory")
			return self.throw_in_failure()
		if not(self.get_datum("throw-in.data-driven")) and self.object.id == "grenouille-000" and self.object2.id == "toilettes-000-bas":
			self.object.move_to(None)
			self.inform("throw-in")
		else:
			return self.throw_in_failure()
			

	def throw_in_failure(self):
		self.fail()
		self.inform("throw-in.failed")
		


